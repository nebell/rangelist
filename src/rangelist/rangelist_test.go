package rangelist

import "testing"

func TestRangeList_Add(t *testing.T) {
	rl := New()
	t.Log("Added [3, 4)")
	_ = rl.Add([2]int{3, 4})
	rl.Print()

	t.Log("Added [7, 16)")
	_ = rl.Add([2]int{7, 16})
	rl.Print()

	t.Log("Added [3, 5)")
	_ = rl.Add([2]int{3, 5})
	rl.Print()

	t.Log("Added [7, 12)")
	_ = rl.Add([2]int{7, 12})
	rl.Print()

	t.Log("Added [4, 12)")
	_ = rl.Add([2]int{4, 12})
	rl.Print()

	t.Log("Added [-20, -16)")
	_ = rl.Add([2]int{-20, -16})
	rl.Print()

	t.Log("Added [-3, 1)")
	_ = rl.Add([2]int{-3, 1})
	rl.Print()

	t.Log("Added [20, 21)")
	_ = rl.Add([2]int{20, 21})
	rl.Print()
}

func TestRangeList_Remove(t *testing.T) {
	rl := New()
	t.Log("Added [3, 4)")
	_ = rl.Add([2]int{3, 4})
	rl.Print()

	t.Log("Removed [2, 3)")
	_ = rl.Remove([2]int{2, 3})
	rl.Print()

	t.Log("Removed [2, 4)")
	_ = rl.Remove([2]int{2, 4})
	rl.Print()

	t.Log("Added [3, 8)")
	_ = rl.Add([2]int{3, 8})
	rl.Print()

	t.Log("Removed [4, 6)")
	_ = rl.Remove([2]int{4, 6})
	rl.Print()

	t.Log("Removed [3, 3)")
	_ = rl.Remove([2]int{3, 3})
	rl.Print()

	t.Log("Removed [4, 4)")
	_ = rl.Remove([2]int{4, 4})
	rl.Print()

	t.Log("Removed [10, 11)")
	_ = rl.Remove([2]int{10, 11})
	rl.Print()

	t.Log("Removed [1, 11)")
	_ = rl.Remove([2]int{1, 11})
	rl.Print()
}
