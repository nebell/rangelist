// Package rangelist structure of range list
package rangelist

import (
	"errors"
	"fmt"
)

var (
	ErrInvalidRangeElement = errors.New("invalid range element")
)

// RangeList implement of range list
type RangeList struct {
	head *node // head of linked list
}

// node a node of linked list
type node struct {
	Val  int
	Next *node
}

// insertTwoNodesOfRange Insert a range between front node and next node
func insertTwoNodesOfRange(leftVal, rightVal int, frontNode, nextNode *node) *node {
	leftNode, rightNode := &node{Val: leftVal, Next: nil}, &node{Val: rightVal, Next: nil}
	frontNode.Next, leftNode.Next, rightNode.Next = leftNode, rightNode, nextNode
	return leftNode
}

// Add add a range to RangeList
func (rangeList *RangeList) Add(rangeElement [2]int) error {

	leftVal, rightVal := rangeElement[0], rangeElement[1]
	if leftVal >= rightVal {
		return ErrInvalidRangeElement
	}

	// empty range list
	if rangeList.head.Next == nil {
		insertTwoNodesOfRange(leftVal, rightVal, rangeList.head, nil)
		return nil
	}

	var leftNode, rightNode *node                         // pointers to mark the nodes
	l, r := rangeList.head.Next, rangeList.head.Next.Next // pointers for iteration
	lastPair := rangeList.head

	// left node of range
	for nil != l && r.Val < leftVal {
		lastPair = l
		l = r.Next
		if nil != l {
			r = l.Next
		}
	}

	switch {
	case nil == l: // end of range list
		insertTwoNodesOfRange(leftVal, rightVal, lastPair.Next, nil)
		return nil
	case leftVal >= l.Val:
		leftNode = l
	case leftVal < l.Val && lastPair == rangeList.head:
		leftNode = insertTwoNodesOfRange(leftVal, rightVal, rangeList.head, l)
	default: // leftVal < l.Val
		leftNode = insertTwoNodesOfRange(leftVal, rightVal, lastPair.Next, l)
	}

	// right node of range
	for nil != l && r.Val < rightVal {
		lastPair = l
		l = r.Next
		if nil != l {
			r = l.Next
		}
	}

	switch {
	case nil == l:
		tailRange := insertTwoNodesOfRange(leftVal, rightVal, lastPair.Next, nil)
		rightNode = tailRange.Next
	case rightVal < l.Val:
		insertRange := insertTwoNodesOfRange(leftVal, rightVal, lastPair.Next, l)
		rightNode = insertRange.Next
	default:
		rightNode = r
	}

	// union
	leftNode.Next = rightNode
	return nil
}

// Remove remove a range to RangeList
func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	leftVal, rightVal := rangeElement[0], rangeElement[1]
	if nil == rangeList.head || nil == rangeList.head.Next {
		return nil
	} else if leftVal >= rightVal {
		return ErrInvalidRangeElement
	}

	var leftNode, rightNode *node
	lastPair, l, r := rangeList.head, rangeList.head.Next, rangeList.head.Next.Next

	for nil != l && r.Val < leftVal {
		lastPair = l
		l = r.Next
		if nil != l {
			r = l.Next
		}
	}

	switch {
	case nil == l || leftVal < l.Val && rightVal < l.Val:
		return nil
	case leftVal > l.Val:
		leftNode = insertTwoNodesOfRange(leftVal, rightVal, l, r)
	case lastPair == rangeList.head:
		leftNode = rangeList.head
	default:
		leftNode = lastPair.Next
	}

	// right node of range
	for nil != l && r.Val < rightVal {
		lastPair = l
		l = r.Next
		if nil != l {
			r = l.Next
		}
	}

	switch {
	case nil == l:
		rightNode = nil
	case r.Val == rightVal:
		rightNode = r.Next
	case rightVal < l.Val:
		rightNode = l
	default:
		pNode := insertTwoNodesOfRange(leftVal, rightVal, l, r)
		rightNode = pNode.Next
	}

	leftNode.Next = rightNode

	return nil
}

// Print print all items of range list
func (rangeList *RangeList) Print() {

	// when empty
	if rangeList.head == nil || rangeList.head.Next == nil {
		fmt.Println("Empty")
		return
	}

	var leftNode, rightNode *node
	leftNode = rangeList.head.Next
	rightNode = leftNode.Next

	for leftNode != nil {
		fmt.Printf("[%d, %d) ", leftNode.Val, rightNode.Val)
		leftNode = rightNode.Next
		if leftNode != nil {
			rightNode = leftNode.Next
		}
	}
	fmt.Println()
}

// New Construct a range list
func New() *RangeList {
	return &RangeList{
		head: &node{Val: 0, Next: nil},
	}
}
